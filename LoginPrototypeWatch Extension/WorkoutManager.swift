//
//  WorkoutManager.swift
//  LoginPrototype
//
//  Created by Matthias Hausner on 20.11.15.
//  Copyright © 2015 Matthias Hausner. All rights reserved.
//

import Foundation
import HealthKit

class WorkoutSessionContext {
    
    let healthStore: HKHealthStore
    let activityType: HKWorkoutActivityType
    let locationType: HKWorkoutSessionLocationType = HKWorkoutSessionLocationType(rawValue: 0)!
    
    init(healthStore:HKHealthStore, activityType: HKWorkoutActivityType = .Other) {
        self.healthStore=healthStore
        self.activityType=activityType
    }
    
}

protocol WorkoutSessionManagerDelegate:class {
    func workoutSessionManager(workoutSessionManager: WorkoutSessionManager, didStartWorkoutWithDate: NSDate)

    func workoutSessionManager(workoutSessionManager: WorkoutSessionManager, didStopWorkoutWithDate: NSDate)

    func workoutSessionManager(workoutSessionManager: WorkoutSessionManager, didUpdateHeartRateSample: HKQuantitySample)
}


class WorkoutSessionManager {

    let healthStore: HKHealthStore
    let workoutSession: HKWorkoutSession
    var workoutSessionStartDate: NSDate
    var workoutSessionEndDate: NSDate
    var queries:[HKAnchoredObjectQuery] = []
    var heartRateSamples: [HKQuantitySample] = []
    let heartRateType = HKObjectType.quantityTypeForIdentifier(HKQuantityTypeIdentifierHeartRate)!
    var currentHeartRateSample: HKQuantitySample?
    
    
    //weak var delegate: WorkoutSessionManagerDelegate?
    
    
   
    
    init(context: WorkoutSessionContext){
        self.healthStore=context.healthStore
        self.workoutSession=HKWorkoutSession(activityType: context.activityType, locationType: context.locationType)
        
        //super.init()
        //self.workoutSession.delegate=self
        self.workoutSessionStartDate = NSDate()
        self.workoutSessionEndDate = NSDate()
        
        
    }
    
    func startWorkout(){
        
        self.healthStore.startWorkoutSession(self.workoutSession) //{success, error in //... */
        
    }
    
    func stopWorkoutAndSave(){
        
    }
    
    func workoutSession(workoutSession: HKWorkoutSession, didChangeToState toState: HKWorkoutSessionState, fromState: HKWorkoutSessionState, date:NSDate){
        
        dispatch_async(dispatch_get_main_queue()) {
            switch toState {
            case .Running:
                self.workoutDidStart(date)
            case .Ended:
                self.workoutDidEnd(date)
                
            default:
                print("Unexpected workout session state")
                }
            }
        }
        
        
        func workoutDidStart(date:NSDate) {
            self.workoutSessionStartDate = date
            
            queries.append(self.createStreamingHeartRateQuery(date))
            
            for query in queries {
                self.healthStore.executeQuery(query)
            }
            //let the interface know that the session has started
            
            //self.delegate?.workoutSessionManager(self, didStartWorkoutWithDate: date)
        }
        
        func workoutDidEnd(date:NSDate){
            
        }
        
        
        func saveWorkout(){
            //Only save a workout if there are valid start and end dates
            
            /*guard let startDate = self.workoutStartDate*/
            
        }
        
    func createStreamingHeartRateQuery(workoutStartDate: NSDate) ->HKAnchoredObjectQuery {
        let predicate = self.predicateForWorkoutSamples(workoutStartDate)
       
        let sampleHandler = { (samples: [HKQuantitySample]) ->Void in
            //Find the most recent heart rate quantity sample
        var mostRecentSample = self.currentHeartRateSample
            var mostRecentStartDate = mostRecentSample?.startDate ?? NSDate.distantPast()
            
            for sample in samples {
                if mostRecentStartDate.compare(sample.startDate) == .OrderedAscending{
                    mostRecentSample=sample
                    mostRecentStartDate=sample.startDate
                }
            }
            
            self.currentHeartRateSample = mostRecentSample
            
            if let sample = mostRecentSample {
                //self.delegate?.workoutSessionManager(self, didUpdateHeartRateSample: sample)
                
            }
            
    }
      
        
        
        
        var samples: [HKSample]? = []
        var deletedObjects: [HKDeletedObject]? = []
        var anchor: HKQueryAnchor? = HKQueryAnchor.init(fromValue: 0)
        var error: NSError?
        var query: HKAnchoredObjectQuery
        //let heartRateQuery: HKAnchoredObjectQuery
        
        
        
      
        
       /* let heartRateQuery = HKAnchoredObjectQuery(type:self.heartRateType, predicate: predicate, anchor: HKQueryAnchor.init(fromValue:0), limit: 0, resultsHandler: (query, samples, deletedObjects, anchor, error))*/
        
        
        let heartRateQuery = HKAnchoredObjectQuery(type: self.heartRateType, predicate: predicate, anchor: anchor, limit: Int(HKObjectQueryNoLimit)) { (query, sampleObjects, deletedObjects, anchor, error) -> Void in
           // guard let newAnchor = newAnchor else {return}
            
        }

        
      
        
      /*if let quantitySamples = heartRateSamples as? [HKQuantitySample]
            {
                sampleHandler(quantitySamples)
                
            }*/
        


        
            return heartRateQuery
        }
    func predicateForWorkoutSamples(startDate:NSDate) -> NSPredicate{ let pred:NSPredicate = NSPredicate()
        return pred
    
    }
}