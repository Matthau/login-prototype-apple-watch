//
//  ChoiceController.swift
//  LoginPrototype
//
//  Created by Matthias Hausner on 18.11.15.
//  Copyright © 2015 Matthias Hausner. All rights reserved.
//

import WatchKit
import Foundation


class ChoiceController: WKInterfaceController {

    @IBOutlet var switchTOTP: WKInterfaceSwitch!
    @IBOutlet var switchSMS: WKInterfaceSwitch!
    
    //state variables for toggle switches
    var switchTOTPStatus:Bool = true
    var switchSMSStatus:Bool = false
    
    //make toggle switches mutually exclusive
    func makeSwitchesMutuallyExclusive(value:Void){
        
        //change switch state on both switches
        switchSMS.setOn(switchTOTPStatus)
        switchTOTP.setOn(!switchTOTPStatus)
        
        //update state variables
        switchSMSStatus = switchTOTPStatus
        switchTOTPStatus = !switchTOTPStatus
    }
    
    override func awakeWithContext(context: AnyObject?) {
        super.awakeWithContext(context)
        
        // Configure interface objects here.
    }

    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
    }

    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }

    
    @IBAction func actionTOTP(value: Bool) {
        
       makeSwitchesMutuallyExclusive()
        
    }
    
    @IBAction func actionSMS(value: Bool) {
        
        makeSwitchesMutuallyExclusive()
    
    }
}
