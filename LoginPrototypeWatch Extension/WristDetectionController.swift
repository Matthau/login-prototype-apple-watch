//
//  WristDetectionController.swift
//  LoginPrototype
//
//  Created by Matthias Hausner on 20.11.15.
//  Copyright © 2015 Matthias Hausner. All rights reserved.
//

import WatchKit
import Foundation
import HealthKit


class WristDetectionController: WKInterfaceController {
    
  
    
     let healthStore: HKHealthStore = HKHealthStore()

    override func awakeWithContext(context: AnyObject?) {
        super.awakeWithContext(context)
        
        // Configure interface objects here.
    }

    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
       
        
        let typesToShare = Set([HKObjectType.workoutType()])
        let typesToRead = Set([HKObjectType.quantityTypeForIdentifier(HKQuantityTypeIdentifierHeartRate)!])
        
        self.healthStore.requestAuthorizationToShareTypes(typesToShare, readTypes: typesToRead) { success, error in /*/...*/}
        
        print("Wrist Detection Requested")
        
    
    }

    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }

}
