//
//  NumberInputController.swift
//  LoginPrototype
//
//  Created by Matthias Hausner on 18.11.15.
//  Copyright © 2015 Matthias Hausner. All rights reserved.
//

import WatchKit
import Foundation


class NumberInputController: WKInterfaceController {
    @IBOutlet var timerTOTP: WKInterfaceTimer!
    
    @IBOutlet var button1: WKInterfaceButton!
    @IBOutlet var button2: WKInterfaceButton!
    @IBOutlet var button3: WKInterfaceButton!
    @IBOutlet var button4: WKInterfaceButton!
    @IBOutlet var button5: WKInterfaceButton!
    @IBOutlet var button6: WKInterfaceButton!
    @IBOutlet var button7: WKInterfaceButton!
    @IBOutlet var button8: WKInterfaceButton!
    @IBOutlet var button9: WKInterfaceButton!
    @IBOutlet var button0: WKInterfaceButton!
    var sourceController: InterController!
    var amount = Double(0)
   
    @IBOutlet var amountLabel: WKInterfaceLabel!
    
    var isDecimalAppended = false
    var isPointZeroAppended = false
    
    let endDate:NSDate = NSDate().dateByAddingTimeInterval(30)
    
    
    
    
        override func awakeWithContext(context: AnyObject?) {
        super.awakeWithContext(context)
            
        
       
        
           
           
            
            
            
            
                
                // Configure interface objects here.
                //sourceController = context as! InterController
                amountLabel.setText("\(self.getDisplayAmount(self.amount))")
            
            
                timerTOTP.start()
                timerTOTP.setDate(endDate)
            
            
            }
    

    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
        
        
    }

    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
        print("didDeactivate")
        
        
    }
    
    func appendValue(value: Int) {
        let newValue = "\(value)"
        var currentValue = getDisplayAmount(self.amount, round: false)
        
        // Handle appending new number
        if currentValue == "0" && !isDecimalAppended {
            // New start of entry
            currentValue = newValue
        }
        else {
            // Handle point zero because of rounding
            if isPointZeroAppended {
                currentValue += currentValue.rangeOfString(".") != nil
                    ? "0" : ".0"
                isPointZeroAppended = false
            }
            
            // Handle intended decimal if applicable
            if isDecimalAppended {
                currentValue += "."
                isDecimalAppended = false
            }
            
            // Handle point zero because of rounding
            if value == 0 && currentValue.rangeOfString(".") != nil {
                isPointZeroAppended = true
            }
            
            // Concatenate intended value
            currentValue += newValue
        }
        
        amountLabel.setText(currentValue)
    }
    
    func getDisplayAmount(value: Double, round: Bool = true) -> String {
        // Truncate decimal if whole number
        return value % 1 == 0
            ? "\(Int(value))"
            : (round ? String(format: "%.5f", value) : "\(value)")
    }

    
    
    
    
    


    @IBAction func actionButton1() {
        print("1")
        appendValue(1)
            }
    @IBAction func actionButton2() {
        print("2")
        appendValue(2)
    }
    @IBAction func actionButton3() {
        print("3")
        appendValue(3)
    }
    @IBAction func actionButton4() {
        print("4")
        appendValue(4)
    }
    @IBAction func actionButton5() {
        print("5")
        appendValue(5)
    }
    @IBAction func actionButton6() {
        print("6")
        appendValue(6)
    }
    @IBAction func actionbutton7() {
        print("7")
        appendValue(7)
    }
    @IBAction func actionButton8() {
        print("8")
        appendValue(8)
    }
    @IBAction func actionButton9() {
        print("9")
        appendValue(9)
    }
    @IBAction func actionButton0() {
        print("0")
        appendValue(0)
    }

    

    
    
}
