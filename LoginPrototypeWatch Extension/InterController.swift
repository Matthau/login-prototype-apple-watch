//
//  InterController.swift
//  LoginPrototype
//
//  Created by Matthias Hausner on 19.11.15.
//  Copyright © 2015 Matthias Hausner. All rights reserved.
//

import WatchKit
import Foundation


class InterController: WKInterfaceController {
    var amount = Double(0)
    
  
    
    @IBOutlet var amountLabel: WKInterfaceLabel!
    override func awakeWithContext(context: AnyObject?) {
        super.awakeWithContext(context)
        
        // Configure interface objects here.
    }
    
    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
        loadData()
    }
    
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }
    
    func loadData() {
        //amountLabel.setText(getDisplayAmount(amount))
    }
    
    func getDisplayAmount(value: Double, round: Bool = true) -> String {
        // Truncate decimal if whole number
        return value % 1 == 0
            ? "\(Int(value))"
            : (round ? String(format: "%.5f", value) : "\(value)")
    }
    
    @IBAction func buttonTapped() {
        // Redirect to number pad
        self.presentControllerWithName("numberPadController", context: self)
    }

}
